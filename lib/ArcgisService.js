/*
### MIT License

Copyright (c) 2020 Muhammad Nizar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*

EXTERNAL LICENSE

### MIT License

Copyright (c) 2019 CartONG

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/**
 * rewrite of CartONG.ArcgisService by mnizarzr
 * for personal purpose
 */

const axios = require('axios')

class ArcgisService {

    constructor(opts) {
        if (typeof opts === 'string' && opts != '') {
            this.url = opts;
        }
        else if (opts.url) {
            this.url = opts.url;
            //TODO: build domain, servicePath and serviceID [low priority]
        }
        else {
            if (!opts.domain || opts.domain == '' || !opts.servicePath || opts.servicePath == '' || !opts.serviceId || opts.serviceId == '') {
                console.error('CartONG.ArcgisService.init: not enough parameters to create url, please check you added either url OR domain, servicePath and serviceID.');
                return;
            }
            else {
                this.domain = opts.domain;
                this.servicePath = opts.servicePath;
                this.serviceId = opts.serviceId;

                this.url = this.domain + this.servicePath + this.serviceId;
            }

        }
        this.token = opts.token || null;
        this.definitionUrl = this.url + '?f=pjson';
        this.mapServiceQueryUrl = '/query?where={where}&text=&objectIds={objectIdList}&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&f=pjson';
        this.maxIdUrl = this.url + '/query?where=1%3D1&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=4326&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=&returnGeometry=true&maxAllowableOffset=&outSR=4326&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=[{\'statisticType\':\'min\',\'onStatisticField\':\'objectid\',\'outStatisticFieldName\':\'objectid_min\'},{\'statisticType\':\'max\',\'onStatisticField\':\'objectid\',\'outStatisticFieldName\':\'objectid_max\'}]&returnZ=false&returnM=false&gdbVersion=&f=pjson';
        opts.name ? this.name = opts.name : console.warn('ArcgisService.init: name not available.');
        this.maxRecordCount = opts.maxRecordCount ? opts.maxRecordCount : ''; //TODO [low priority]: to get maxRecordCount and other meta (SR, geometry type...) from the service itself

        //this.definitionPromise = this.loadDefinition();
        this.definition;

        this.defaultQueryParams = {
            where: '1=1',
            text: null,
            objectIds: null,
            time: null,
            geometry: null,
            geometryType: 'esriGeometryEnvelope',
            geometryPrecision: 6,
            inSR: null,
            outSR: null, //4326,
            spatialRel: 'esriSpatialRelIntersects',
            relationParam: null,
            maxAllowableOffset: null,
            outFields: '*',
            orderByFields: null,
            groupByFieldsForStatistics: null,
            outStatistics: null,
            gdbVersion: null,
            datumTransformation: null,
            parameterValue: null,
            rangeValues: null,
            resultOffset: 0,
            resultRecordCount: null,
            returnGeometry: true,
            returnZ: false,
            returnM: false,
            returnIdsOnly: false,
            returnCountOnly: false,
            returnDistinctValues: false,
            returnTrueCurves: false,
            returnExtentsOnly: false,
            queryByDistance: null,
            f: 'json' //'geojson'
        }
    }

    async loadDefinition() {
        return new Promise(async (resolve, reject) => {
            await axios.get(this.definitionUrl, { responseType: 'json' })
                .then(response => {
                    if (response.error) {
                        promise.reject({
                            error: { message: 'Service error: ' + response.error.message + '. Token secured services are not supported yet, coming soon.' }
                        });
                    }
                    else {
                        this.definition = response;
                        resolve(response);
                    }
                }).catch(err => {
                    var msgError = 'Service error'
                    msgError += (err.status == 404) ? (': ' + err.statusText + '.') : '.';
                    reject({
                        error: { message: msgError }
                    });
                })
        })
    }

    async getData(params) {
        return new Promise(async (resolve, reject) => {
            this.query(params)
                .then(json => {
                    if (json.features === undefined) {
                        reject({
                            error: { message: 'No features in the result.' }
                        });
                        return;
                    }
                    //promise.resolve(json.features);
                    resolve(json);
                })
                .catch(err => {
                    const msgError = (err.error ? err.error.message : '')
                    reject({
                        error: { message: 'Service error' + (msgError != '' ? ': ' + msgError : '') + '.' }
                    });
                });
        })
    }

    query(params) {
        return new Promise((resolve, reject) => {
            //var queryParams = {}
            params = params || {};
            params.resultOffset = 0;

            this.queryNextFeatures(params, [], resolve, reject);
        })
    }

    async queryNextFeatures(params, features, resolve, reject) {

        var maxObjects = this.definition.maxRecordCount || 995;

        const queryUrl = this.getEsriURL({
            url: this.url,
            params: params
        });

        await axios.get(queryUrl).then(res => {

            if (res.error) {
                //debugger
                reject(res)
            }
            else {

                res.data.features.forEach(feature => {
                    features.push(feature);
                });

                if (res.data.features.length === maxObjects) {
                    params.resultOffset += maxObjects;
                    this.queryNextFeatures(params, features, resolve, reject);
                } else {

                    var output = res;
                    output.features = features;

                    resolve(output);

                }

            }
        }).catch(err => {
            console.log(err)
            reject(err)
        })

    }

    getEsriURL(endpoint, params) {
        var
            query = '',
            separator = '',
            key, val;

        endpoint.params = endpoint.params || {};

        for (key in this.defaultQueryParams) {
            val = typeof endpoint.params[key] !== 'undefined' ? endpoint.params[key] : this.defaultQueryParams[key];
            if (val !== '' && val !== null) {
                query += separator + key + '=' + encodeURIComponent(this.setParams(val, params));
                separator = '&';
            }
        }

        return this.setParams(endpoint.url, params) + '/query?' + query;
    };

    setParams(str, params) {
        //return (str || '').replace(/\{(\w+)\}/g, function(tag, key) {
        //  return params[key] !== undefined ? params[key] : tag;
        //});
        if (!params) {
            return str;
        }
        return ('' + str).replace(/\{([^\{\}]+)\}/g, function (t, k) {
            return params[k] !== undefined ? params[k] : t;
        });
    };

}

module.exports = ArcgisService