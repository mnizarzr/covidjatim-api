const { db } = require('../instances')

const getFaqs = (keys, redis) => {

    const sql = `SELECT * FROM faq`

    db.query(sql, (error, results, fields) => {
        redis.set(keys.faq, JSON.stringify(results))
    })

}

const getLinks = (keys, redis) => {

    const sql = `SELECT l.id, l.name, l.url, lc.name AS category FROM link l INNER JOIN link_category lc ON l.category_id = lc.id`

    db.query(sql, (error, results, fields) => {
        redis.set(keys.link, JSON.stringify(results))
    })

}

module.exports = { getFaqs, getLinks }