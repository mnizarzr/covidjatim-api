const axios = require('axios')
const cheerio = require('cheerio')
const moment = require('moment')
const { db } = require('../instances')
const d3 = require('d3-geo')
const { features } = require('../lib/jatim_geo.json')
const jatimDistricts = require('../lib/jatim_districts.json')

async function scrapeDetail($) {

    const text = $('script')[9].children[0].data
    const dataKabupaten = text.match(/var datakabupaten(.*];\n)?/);
    const stringJson = dataKabupaten[0].substring(18).slice(0, -2)

    return stringJson

}

async function parseHtmlTabletoJson($) {
    let data = []

    await $('table').children('tbody').children('tr').each((i, el) => {

        const tds = $(el).find("td")
        let district_name = $(tds[0]).text(),
            odr = $(tds[1]).text(),
            otg = $(tds[2]).text(),
            odp = $(tds[3]).text(),
            pdp = $(tds[4]).text(),
            confirmed = $(tds[5]).text(),
            updated_at = $(tds[6]).text();

        district_name = district_name.replace(/KAB./, "KABUPATEN")

        const obj = { district_name, odr, otg, odp, pdp, confirmed, updated_at }
        data.push(obj)
    })

    return data
}

const getDistrict = async (keys, redis) => {

    let response

    try {
        response = await axios.get("http://covid19dev.jatimprov.go.id/xweb/draxi")
        if (response.status !== 200) console.log(response.statusText)
    } catch (e) {
        console.log("ERROR", e)
        return null
    }

    const html = cheerio.load(response.data)
    const result = await parseHtmlTabletoJson(html)
    const result_detail = await scrapeDetail(html)

    jatimDistricts.map(el => {
        result.map(element => {
            if (element.district_name === el.name) {
                element.district_id = el.id
            }
        })
    })

    redis.set(keys.district, JSON.stringify(result))
    redis.set(keys.district_detail, result_detail)

}

const getDistrictByLocation = async (data, location) => {

    location = location.split(',')
    const latitude = location[0]
    const longitude = location[1]

    let userDisctrictId

    const geometries = features

    geometries.map(el => {

        if (d3.geoContains(el, [longitude, latitude])) {
            userDisctrictId = el.properties.id.replace(".", "")
        }

    })

    const obj = {
        province: data.provinces.filter(el => el.province_id === 35 || el.province_name === "Jawa Timur")[0],
        district: data.districts.filter(el => el.district_id == userDisctrictId)[0]
    }

    return obj

}

const getDetailByDate = async (data, date) => {

    const newData = []

    data.map(el => {
        if (moment(el.updated_at).format("YYYY-MM-DD") === date) {
            newData.push(el)
        }
    })

    return newData

}

module.exports = { getDistrict, getDistrictByLocation, getDetailByDate }