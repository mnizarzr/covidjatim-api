const getGlobal = require('./getGlobal'),
    district = require('./getDistrict'),
    getProvince = require('./getProvince'),
    hospital = require('./hospital')
const { getLinks, getFaqs } = require('./getOther')


module.exports = { getGlobal, district, getProvince, hospital, getLinks, getFaqs }