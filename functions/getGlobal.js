const axios = require('axios'),
moment = require('moment-timezone')

const getGlobal = async (keys, redis) => {

    let result

    await axios.all([
        axios.get("https://corona.lmao.ninja/countries/id"),
        axios.get("https://corona.lmao.ninja/all")
    ]).then(axios.spread((...responses) => {
        result = responses[0].data
        result.global = responses[1].data
    }))

    result.updated = moment(result.updated).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss')
    result.global.updated = moment(result.global.updated).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss')

    redis.set(keys.global, JSON.stringify(result))

}

module.exports = getGlobal