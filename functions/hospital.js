const { db, redis, config } = require('../instances')
const moment = require('moment-timezone')
const d3 = require('d3-geo')
const jatimGeoJson = require('../lib/jatim_geo.json')

const getAllHospital = async (keys, redis) => {

    const sql = `SELECT * FROM hospital`

    db.query(sql, (error, results, fields) => {
        if (error) throw error
        redis.set(keys.hospital, JSON.stringify(results))
    })

}

const getHospitalByLocation = async (location) => {

    location = location.split(',')
    const latitude = location[0]
    const longitude = location[1]

    let userDisctrictId

    let geometries = jatimGeoJson.features

    geometries.map(el => {

        if (d3.geoContains(el, [longitude, latitude])) {
            userDisctrictId = el.properties.id.replace(".", "")
        }

    })

    const results = JSON.parse(await redis.get(config.keys.hospital))

    const newResult = {}
    newResult.primary = results.filter(el => el.id == 1 || el.id == 2 || el.id == 3)
    newResult.in_city = results.filter(el => el.district_id == userDisctrictId)

    return newResult

}

const searchHospital = async (keyword) => {

    const results = JSON.parse(await redis.get(config.keys.hospital))

    const newResult = results.filter(obj => obj.name.toLowerCase().includes(keyword.toLowerCase()))

    return newResult

}

const addHospital = async (req) => {
    return new Promise((resolve, reject) => {

        let { district_id, name, address, phone, location } = req

        location = location.split(",")

        district_id = parseInt(district_id)
        // latitude = parseFloat(latitude)
        // longitude = parseFloat(longitude)

        const dateTimeNow = moment.tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss')

        const sql = `INSERT INTO hospital VALUES(NULL, :district_id, :name, :address, :phone, :latitude, :longitude, :created_at, :updated_at)`

        db.query(sql, {
            district_id, name, address, phone, latitude: location[0], longitude: location[1], created_at: dateTimeNow, updated_at: dateTimeNow
        }, (error, results, fields) => {
            if (error) reject(error)
            else if (results) resolve("Hospital added")
        })

    })
}

const editHospital = async (req) => {
    return new Promise((resolve, reject) => {

        let { id, district_id, name, address, phone, latitude, longitude } = req

        id = parseInt(id)
        district_id = parseInt(district_id)
        latitude = parseFloat(latitude)
        longitude = parseFloat(longitude)

        const dateTimeNow = moment.tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss')

        const sql = `UPDATE hospital SET district_id = :district_id, name = :name, address = :address,
        phone = :phone, latitude = :latitude, longitude = :longitude, updated_at = :updated_at WHERE id = :id`

        db.query(sql, {
            id, district_id, name, address, phone, latitude, longitude, updated_at: dateTimeNow
        }, (error, results, fields) => {
            if (error) reject(error)
            else if (results) resolve("Hospital edited")
        })

    })
}


module.exports = { addHospital, editHospital, getAllHospital, getHospitalByLocation, searchHospital }