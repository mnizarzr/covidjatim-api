const ArcgisService = require('../lib/ArcgisService')
const moment = require('moment-timezone')
const axios = require('axios')

async function getResult() {
    return new Promise(async (resolve, reject) => {

        var serviceUrl = "https://services5.arcgis.com/VS6HdKS0VfIhv8Ct/ArcGIS/rest/services/COVID19_Indonesia_per_Provinsi/FeatureServer/0/"

        var arcgisService = new ArcgisService(serviceUrl)
        var params = {
            returnGeometry: false,
            f: "geojson"
        }

        await arcgisService.loadDefinition()
            .then(() => {
                arcgisService.getData(params)
                    .then(res => {
                        resolve(res)
                    })
                    .catch(err => {
                        console.log(err)
                        reject(err)
                    })
            })
            .catch(err => {
                console.log(err)
                reject(err)
            })

    })
}

const getProvince = async (keys, redis) => {

    var data
    var response
    var result = {}

    await getResult()
        .then(res => data = res)
        .catch(err => console.log(err))

    if (data == null) {
        try {
            response = await axios.get("https://api.kawalcorona.com/indonesia/provinsi/")
            if (response.status !== 200) console.log(response.statusText)
        } catch (e) {
            console.log(e); return null
        }
    }

    if (response != null) {
        response.data.map(el => {
            el.properties = el.attributes
            delete el.attributes
        })
    }

    const newData = data == null ? response.data : data.features

    result.provinces = []

    // const indonesia = {
    //     id: 35,
    //     province_id: 0,
    //     province_name: "Indonesia",
    //     confirmed: 0,
    //     recovered: 0,
    //     deaths: 0
    // }

    // data.features.splice(-1, 1)
    newData.map(el => {

        el.properties["id"] = el.properties['FID']
        delete el.properties['FID']

        el.properties["province_id"] = el.properties['Kode_Provi']
        delete el.properties['Kode_Provi']

        el.properties["province_name"] = el.properties['Provinsi']
        delete el.properties['Provinsi']

        el.properties["confirmed"] = el.properties['Kasus_Posi']
        delete el.properties['Kasus_Posi']

        el.properties["recovered"] = el.properties['Kasus_Semb']
        delete el.properties['Kasus_Semb']

        el.properties["deaths"] = el.properties['Kasus_Meni']
        delete el.properties['Kasus_Meni']

        // indonesia.confirmed += el.properties["confirmed"]
        // indonesia.recovered += el.properties["recovered"]
        // indonesia.deaths += el.properties["deaths"]

        result.provinces.push(el.properties)
    })

    // result.data.push(indonesia)

    const modified = data == null ? moment(response.headers.date).format('YYYY-MM-DD HH:mm:ss') : moment(data.headers['last-modified']).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss')
    result.last_modified = modified  // won't be accurate, this is the modified date of cache server

    redis.set(keys.province, JSON.stringify(result))
}

module.exports = getProvince