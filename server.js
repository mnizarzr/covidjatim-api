const cors = require('cors')
const bodyParser = require('body-parser')
const express = require('express'),
    app = express();

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const { redis, config } = require('./instances');
const { keys } = config;

const { district, getProvince, getGlobal, hospital, getLinks, getFaqs } = require('./functions')
const { getDistrict, getDistrictByLocation, getDetailByDate } = district
const { addHospital, editHospital, getAllHospital, getHospitalByLocation, searchHospital } = hospital

const executeAll = () => {
    getDistrict(keys, redis)
    getProvince(keys, redis)
    getGlobal(keys, redis)
    getAllHospital(keys, redis)
    getLinks(keys, redis)
    getFaqs(keys, redis)
}

executeAll()
setInterval(executeAll, config.interval);

const listener = app.listen(config.port, () => {
    console.log(`Service running on port ${listener.address().port}`)
})

app.get('/jatim', async (req, res) => {
    const result = await redis.get(keys.district)
    const json = JSON.parse(result)
    res.send(json)
})

app.get('/provinces', async (req, res) => {
    const result = await redis.get(keys.province)
    const json = JSON.parse(result)
    res.send(json)
})

app.get('/global', async (req, res) => {
    const result = await redis.get(keys.global)
    const json = JSON.parse(result)
    res.send(json)
})

app.route('/hospitals')
    .get(async (req, res) => {
        const result = await redis.get(keys.hospital)
        const json = JSON.parse(result)
        res.send(json)
    })
    .post(async (req, res) => {
        res.send(await addHospital(req.body))
    })
    .put(async (req, res) => {
        res.send(await editHospital(req.body))
    })

app.get('/hospitals/:location', async (req, res) => {
    const result = await getHospitalByLocation(req.params.location)
    res.send(result)
})

app.get('/search/hospitals?', async (req, res) => {
    const result = await searchHospital(req.query.q)
    res.send(result)
})

app.get('/district/:location', async (req, res) => {
    let data = JSON.parse(await redis.get(keys.province))
    data.districts = JSON.parse(await redis.get(keys.district))
    const result = await getDistrictByLocation(data, req.params.location)
    res.send(result)
})

app.get('/district?', async (req, res) => {
    const data = JSON.parse(await redis.get(keys.district_detail))
    const result = await getDetailByDate(data, req.query.date)
    res.send(result)
})

app.get('/links', async (req, res) => {
    const result = await redis.get(keys.link)
    const json = JSON.parse(result)
    res.send(json)
})

app.get('/faq', async (req, res) => {
    const result = await redis.get(keys.faq)
    const json = JSON.parse(result)
    res.send(json)
})