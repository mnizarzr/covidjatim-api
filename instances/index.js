const db = require('./database'),
    redis = require('./redis'),
    config = require('../config.json')

module.exports = { db, redis, config }